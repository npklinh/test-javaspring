package producer.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "producer")
public class ProducerEntity {
    @NotNull
    @Column(name = "pro_id")
    private String proid;

    @Column(name ="pro_name")
    private  String proname;
    @Column(name="pro_address")
    private String proadress;
    @Column(name="pro_phone")
    private  String prophone;

    public ProducerEntity(String proid, String proname, String proadress, String prophone) {
        this.proid = proid;
        this.proname = proname;
        this.proadress = proadress;
        this.prophone = prophone;
    }

    public String getProid() {
        return proid;
    }

    public void setProid(String proid) {
        this.proid = proid;
    }

    public String getProname() {
        return proname;
    }

    public void setProname(String proname) {
        this.proname = proname;
    }

    public String getProadress() {
        return proadress;
    }

    public void setProadress(String proadress) {
        this.proadress = proadress;
    }

    public String getProphone() {
        return prophone;
    }

    public void setProphone(String prophone) {
        this.prophone = prophone;
    }
}
