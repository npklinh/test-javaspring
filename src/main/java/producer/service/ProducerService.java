package producer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import producer.entity.ProducerEntity;
import producer.repository.ProducerRepository;

import java.util.List;

@Service
public class ProducerService {

    @Autowired
    private ProducerRepository producerRepository;

    public List<ProducerEntity> findAll() {

            List<ProducerEntity> test = producerRepository.findAll();
            if (test == null)
                return null;

        return test;
    }
}
