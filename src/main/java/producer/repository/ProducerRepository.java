package producer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import producer.entity.ProducerEntity;
@Repository
public interface ProducerRepository extends JpaRepository<ProducerEntity,String> {
}
