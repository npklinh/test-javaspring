package producer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import producer.entity.ProducerEntity;
import producer.repository.ProducerRepository;
import producer.service.ProducerService;

import java.util.List;

@Controller
public class ProducerController {

    @Autowired
    private ProducerService producerService;



    @GetMapping("/getall")
    public List<ProducerEntity> getall() {
        return producerService.findAll();
    }


}
