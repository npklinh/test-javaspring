import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan({ "producer" })
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "producer.repository" })
@EnableJpaAuditing
public class DatabaseConfiguration {
}
