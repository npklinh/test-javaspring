import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.net.UnknownHostException;

@SpringBootApplication(scanBasePackages={"producer"})

public class Main{
    private Object scanBasePackages;

    public  static void main(String [] args){
        SpringApplication.run(Main.class,args);

    }
}